Details
----------
This is a fork of bastibe/lunatic-python which can be found on the 'net at https://github.com/bastibe/lunatic-python'. An older original one is Lunatic Python, which can be found on the 'net at http://labix.org/lunatic-python. This is an updated version of lunatic-python that works with Python 2.7 and Lua 5.1.


Author Contact
----------
	
Fangyi Zhang   email:gzzhangfangyi@gmail.com
	
ARC Centre of Excellence for Robotic Vision (ACRV)
	
Queensland Univsersity of Technology (QUT)


Installing
----------
To install, you will need to have the Python and Lua development libraries on your system.
(This version has been modified to compile under Ubuntu 14.04 and 16.04. I haven't tested it under other
distributions, your mileage may vary.)

    Option 1:
        run 'install.sh' to install python and lua dependencies and this package autonomously.

	Option 2:
		Download the file package (e.g., lunatic-python-bugfixed.zip)
		cd the-path-where-the-package-is-in
		run "sudo pip install the-package-name" (e.g., sudo pip install lunatic-python-bugfixed.zip)
		extract the package and copy the file "python.lua" to the directory where lua requires files from.
		update the directory path for lua-python.so in the file "python.lua"
			(it is normally in "/usr/local/lib/python2.7/dist-packages/lua-python.so")
		require "python.lua" in the lua file you want to use python
		use in accordance to the lunatic-python manual
		
	Option 3:
		Download the file package (e.g., lunatic-python-bugfixed.zip)
		cd the-path-where-the-package-is-in
		extract extract the package
		cd the root path of the package
		run "cmake ."
		run "sudo ./setup.py install"
		copy the file "python.lua" to the directory where lua requires files from.
		update the directory path for lua-python.so in the file "python.lua"
			(it is normally in "/usr/local/lib/python2.7/dist-packages/lua-python.so")
		require "python.lua" in the lua file you want to use python
		use in accordance to the lunatic-python manual


User Manual
----------
	Lunatic-python-manual.pdf


