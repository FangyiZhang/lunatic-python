#!/usr/bin/env bash

sudo apt-get install -qqy python2.7
sudo apt-get install -qqy python2.7-dev
sudo apt-get install -qqy lua5.1
sudo apt-get install -qqy lua5.1-dev

cmake .
sudo ./setup.py install
RET=$?; if [ $RET -ne 0 ]; then echo "Error. Exiting."; exit $RET; fi
echo "lunatic-python installation completed"
cp python.lua $TORCH_PATH/share/lua/5.1/
RET=$?; if [ $RET -ne 0 ]; then echo "Error. Exiting."; exit $RET; fi
echo
echo "Installed lunatic-python successfully!"
echo
echo "You can import it by adding 'require python.lua' in a lua script."
echo
echo "If necessary, please manually update the directory path for lua-python.so in the file 'python.lua'!"
echo "(It is '/usr/local/lib/python2.7/dist-packages/lua-python.so') by default.)"
echo
